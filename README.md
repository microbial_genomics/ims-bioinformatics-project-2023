```
IMS Bioinformatics Assessment 2023                        A-T
 ______    __  ___  __          __   ___  ___    ______  T---A
|   _  \  |  |/  / |  |        |  | |   \/   |  /  ____| G---C
|  |_)  | |     /  |  |  ____  |  | |        | |  (___    T-A
|      /  |    |   |  | |____| |  | |  |\/|  |  \___  \    T
|  |\  \  |     \  |  |        |  | |  |  |  |  ____)  |  C-G
|__| \__| |__|\__\ |__|        |__| |__|  |__| |______/  T---A
                                                         C---G
- Application project for IMS Bioinformaticians -         G-C
```
## Introduction

Thank you for your application at MF1 - the Genome Competence Center of the RKI. This project has been designed to test your skills and abilities in regards to solving a typical problem associated with the genomic surveillance of bacterial and viral pathogens. In the following, you will be presented with a task which requires your skills as a bioinformatician to be solved. Please work through the instructions below in preparation for your personal interview.

## About yourself

Before we begin we are interested to learn more about yourself. For this, we have prepared a small self-evaluation task. Please download the radar plot excel table from this repository and perform a self-evaluation on the set of given categories. Keep the radar plot for now. You will not be required to present this, however we may use this as a basis for further discussion during the interview.

## Perform an outbreak analysis

*The following fictional scenario represents an oversimplification of actual outbreak events. Any similarity to real events is purely coincidental and is not intended by any means. The data utilized for this has no background associated with the fictional scenario and is used for demonstration purposes only.*

Imagine the following scenario: You are called upon in the early morning hours of a Monday to be informed about a potential outbreak of pathogenic *E. coli* occurring across various locations. Amongst the first lines of action, samples of 108 hospitalized patients are collected and are short-read shotgun sequenced for further investigation by experts at the RKI. The core bioinformatician team has already processed these data into reconstructed genomes and uploaded them to NCBI for your analysis (see list of assemblies in the repository). You are requested to provide the following information in a presentable form:

+ How closely related is the sample set? Does this represent a single outbreak event or multiple, independent lineages?
+ Due to the decision to rapidly sequence a wide range of patients, it's likely that a majority of isolates aren't actually harmful. Can you distinguish between the pathogenic and non-pathogenic lineages of the data set?
+ Can you pinpoint any genomic component(s) associated with the pathogenicity of these isolates?

To tackle this task you are free to perform any analysis you deem necessary with any software, programming language(s) or additional data sets of your choice. Please prepare a one-slide showcase of your approach and results for the upcoming personal interview. You will be given sufficient time (free discussion of approximately 10min) to present these during our meeting.

## Submitting your results for the interview

Once you have generated some results, please take a look at the following points on how to submit them to us prior to your interview:

+ Prepare a slide to present your approach and results in the upcoming interview
+ Create a private fork of this repository, invite @SWolf95 as a member and set permissions of this user to "Developer"
+ Push your personalized radar plot, your current results, any code or scripts you generated and your slides into the fork

Please make sure to **submit everything at least 24 hours prior to your interview**. You can continue working on your results after this timeframe, but try to submit at least parts of your results early on, as these essentially allow us to take a look at some of your progress beforehand. If we don't receive access to your fork 24 hours prior to the interview, we will contact you to request access.

## FAQ

**I don't possess the (computational) resources required for this kind of analysis, what can I do?**

There are many ways to tackle the aforementioned task, even using low-end hardware or many of the available browser-based platforms. If this is still too limiting, you may subsample the data set into a more handleable set by yourself. Please specify how you performed any kind of subsampling or isolate selection during your presentation.

**Are there any limitations on how much I can present during the interview? Can you specify the presentation format in more detail?**

During the interview, you will be asked to share your computer screen through the WebEx platform to present your results. You are principally free to present your results in whatever format you prefer (slide, interactive image, etc.), however the presentation timeslot itself is limited to a total of 10min, including questions and free discussion. Therefore, we ask that you please condense your results as much as possible (e.g. 1 slide with major points) in order to allow sufficient time for a joint discussion.

**Are there any (clinical) metadata available for these sequences?**

We will not provide any additional metadata on these sequences, however you may utilize the NCBI platform to retrieve some limited metadata on these sequences.

## Contact

If you have any questions throughout this process, feel free to contact us directly (wolfs(at)rki.de). We are eager to see how you will approach this challenge. Good luck!